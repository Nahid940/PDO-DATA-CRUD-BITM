<?php
    try{
        $conn=new PDO('mysql:host=localhost;dbname=userdata101',"root","");
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }catch(PDOException $exp){
        echo $exp->getMessage();
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <style>
        .width{
            width: 50%;
        }
    </style>
</head>
<body>
 <a href="classtest.php">Insert</a>
  <div class="container">
    <table class="table">
       <tr>
           <th>Name</th>
           <th>Email</th>
           <th>Address</th>
           <th>Mobile</th>
           <th>Gender</th>
           <th>Hobby</th>
           <th>DOB</th>
           <th>Image</th>
           <th>Edit</th>
       </tr>
       
       <?php
        $sql="select * from user";
        $stmt=$conn->prepare($sql);
        $stmt->execute();
        $result=$stmt->setFetchMode(PDO::FETCH_ASSOC);
        foreach($stmt->fetchAll() as $val){
            
        
        
//            $sql="insert into user (name,email,address,mobile,gender,hobby,dob,image) values()"
            
        ?>
       <tr>
           <td><?php echo $val['name']?></td>
           <td><?php echo $val['email']?></td>
           <td><?php echo $val['address']?></td>
           <td><?php echo $val['mobile']?></td>
           <td><?php echo $val['gender']?></td>
           <td><?php echo $val['hobby']?></td>
           <td><?php echo $val['DOB']?></td>
           <td><img src="<?php echo $val['image']?>" alt=""></td>
           <td><a href="edit.php?id=<?php echo $val['id']?>" class="btn btn-info">Edit</a></td>
       </tr>
       <?php }?>
   </table>
  </div>

    
</body>
</html>